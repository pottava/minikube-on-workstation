Minikube on Cloud Workstations
===

## Skaffold の場合

### 1. サンプルアプリのデプロイ

```sh
skaffold dev
```

### 2. サンプルアプリへのアクセス

Workstations のゲートウェイ URL にアクセスし、以下のパスを試してください。

- https://8080-ws.cluster-xxx.cloudworkstations.dev/
- https://8080-ws.cluster-xxx.cloudworkstations.dev/req
- https://8080-ws.cluster-xxx.cloudworkstations.dev/sleep/2000
- https://8000-ws.cluster-xxx.cloudworkstations.dev/
- https://8000-ws.cluster-xxx.cloudworkstations.dev/req
- https://8000-ws.cluster-xxx.cloudworkstations.dev/errors/500


## kubectl の場合

### 1. プロキシのビルド

```sh
eval $(minikube docker-env)
docker build -t envoy-custom:v0.1 envoy
docker build -t nginx-custom:v0.1 nginx
```

### 2. サンプルアプリのデプロイ

```sh
kubectl apply -f apps/manifest.yaml
```

### 3. ローカルホストへポートフォワード

```sh
kubectl port-forward svc/envoy 8080:8080
kubectl port-forward svc/nginx 8000:80
```

### 4. サンプルアプリへのアクセス

Workstations のゲートウェイ URL にアクセスし、以下のパスを試してください。

- https://8080-ws.cluster-xxx.cloudworkstations.dev/
- https://8080-ws.cluster-xxx.cloudworkstations.dev/req
- https://8080-ws.cluster-xxx.cloudworkstations.dev/sleep/2000
- https://8000-ws.cluster-xxx.cloudworkstations.dev/
- https://8000-ws.cluster-xxx.cloudworkstations.dev/req
- https://8000-ws.cluster-xxx.cloudworkstations.dev/errors/500
